using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Media;

namespace space_shootout
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {

        
        SoundEffect remix;
        
        
        SpriteFont font;
        SpriteFont font2;
        SpriteFont font3;
        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        Texture2D sprite;
        Vector2 spritePosition;
        bool playing =true;
        
        Texture2D sprite2;
        Vector2 sprite2Position;
        
        
        Texture2D bullet;
        Vector2 bulletPosition;
        bool shooting;
        SoundEffect gunshot;

        Texture2D bullet2;
        Vector2 bullet2Position;
        bool shooting2;
        SoundEffect gunshot2;

        bool colliding = false;
        bool colliding2 = false;
        
        Texture2D background;

        Texture2D background2;
         
        
        int score1;
        int score2;
        
        
        KeyboardState state;
       
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            sprite = Content.Load<Texture2D>("player1");
            spritePosition = new Vector2(0, 0);

            sprite2 = Content.Load<Texture2D>("player2");
            sprite2Position = new Vector2(0, Window.ClientBounds.Height - sprite2.Bounds.Height);

            bullet = Content.Load<Texture2D>("fireball");

            bullet2 = Content.Load<Texture2D>("barf");
           
            background = Content.Load<Texture2D>("space");

            font = Content.Load<SpriteFont>("scorefont");

            font2 = Content.Load<SpriteFont>("scorefont");

            font3 = Content.Load<SpriteFont>("scorefont");

            gunshot = Content.Load<SoundEffect>("shotgun");

            gunshot2 = Content.Load<SoundEffect>("shotgun");

            remix = Content.Load<SoundEffect>("music");

            background2 = Content.Load<Texture2D>("screen");

            

            
            bullet2Position = new Vector2(10000, 10000);


            running = true;
            remix.Play();

           

          
            score1 = 0;
            score2 = 0;

            

        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            spritePosition.X = MathHelper.Clamp(spritePosition.X, 0, Window.ClientBounds.Width - sprite.Bounds.Width);

            Rectangle spriteRect = new Rectangle((int)spritePosition.X, (int)spritePosition.Y,
                sprite.Bounds.Width, sprite.Bounds.Height);

            sprite2Position.X = MathHelper.Clamp(sprite2Position.X, 0, Window.ClientBounds.Width - sprite2.Bounds.Width);

            Rectangle sprite2Rect = new Rectangle((int)sprite2Position.X, (int)sprite2Position.Y,
                sprite2.Bounds.Width, sprite2.Bounds.Height);

            Rectangle bulletRect = new Rectangle((int)bulletPosition.X, (int)bulletPosition.Y,
               bullet.Bounds.Width, bullet.Bounds.Height);

            Rectangle bullet2Rect = new Rectangle((int)bullet2Position.X, (int)bullet2Position.Y,
              bullet2.Bounds.Width, bullet2.Bounds.Height);
            
            if( bullet2Rect.Intersects(spriteRect) && colliding == false)
            {
                colliding = true;
                score2 ++; 
               
            }
            else if (!bullet2Rect.Intersects(spriteRect) && colliding == true)
            {
                colliding = false;
            }

            if (bulletRect.Intersects(sprite2Rect) && colliding2 == false)
            {
                colliding2 = true;
                score1++;
            }
            else if (!bulletRect.Intersects(sprite2Rect) && colliding2 == true)
            {
                colliding2 = false;
            }

            
            
          
            
            // TODO: Add your update logic here

            state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.A))
            {
                spritePosition.X -= 1.0f * gameTime.ElapsedGameTime.Milliseconds;

            }
            if (state.IsKeyDown(Keys.D))
            {
                spritePosition.X += 1.0f * gameTime.ElapsedGameTime.Milliseconds;
            }

            if (!shooting)
            {
                if (state.IsKeyDown(Keys.W))
                {
                    bulletPosition = spritePosition;
                    shooting = true;

                    gunshot.Play();
               
                }
            }
                    
            if (!shooting2)
            {
                if (state.IsKeyDown(Keys.Space))
                {
                    bullet2Position = sprite2Position;
                    shooting2 = true;

                    gunshot.Play();
                
                }

            }

            if (shooting2)
            {
                bullet2Position.Y -= 1.0f * gameTime.ElapsedGameTime.Milliseconds;
                if (bullet2Position.Y < 0)
                {
                    shooting2 = false;
                    bullet2Position = new Vector2(10000, 10000);
                }
            }

            if (shooting)
            {
                bulletPosition.Y += 1.0f * gameTime.ElapsedGameTime.Milliseconds;
                if (bulletPosition.Y > Window.ClientBounds.Height)
                {
                    shooting = false;

                    
                
                }
            }

           //sprite2

            if (state.IsKeyDown(Keys.J))
            {
                sprite2Position.X -= 1.0f * gameTime.ElapsedGameTime.Milliseconds;

            }
            if (state.IsKeyDown(Keys.L))
            {
                sprite2Position.X += 1.0f * gameTime.ElapsedGameTime.Milliseconds;
            }
            
            base.Update(gameTime);
        }
            
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            

            spriteBatch.Begin();

           // Color colors[] = new Color[] {Color.White,Color.Red};

            if(playing)
            {

                if (score1 >= 20 || score2 >= 20)
                {
                    spriteBatch.Draw(background, new Vector2(0, 0), Color.Red);
                }
                else
                { 
                    spriteBatch.Draw(background, new Vector2(0, 0), Color.White);
                }
                
                
                spriteBatch.DrawString(font, "red score = " + score1, new Vector2(345, 150), Color.White);

                spriteBatch.DrawString(font2, "purple score = " + score2, new Vector2(315, 200), Color.White);
                
                spriteBatch.Draw(sprite, spritePosition, Color.White);

                spriteBatch.Draw(sprite2, sprite2Position, Color.White);

                if (shooting)
                {
                    spriteBatch.Draw(bullet, bulletPosition, Color.White);

                }

                if (shooting2)
                {
                    spriteBatch.Draw(bullet2, bullet2Position, Color.White);
                }


            }

            if(!playing)
            {
                


            }


            if (score1 >= 25)
            {
                
                
                shooting = false;
                shooting2 = false;
                spriteBatch.Draw(background2, new Vector2(0, 0), Color.White);
                spriteBatch.DrawString(font3, "Red Wins ", new Vector2(345, 270), Color.Yellow);
            }


            if (score2 >= 25)
            {
                
                
                shooting = false;
                shooting2 = false;
                spriteBatch.Draw(background2, new Vector2(0, 0), Color.White);
                spriteBatch.DrawString(font3, "Purple Wins  ", new Vector2(345, 270), Color.Yellow);
            }
            
           
            
            
            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        public bool running { get; set; }
    }
}
