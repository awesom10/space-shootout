﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace Game1
{
    class Sprite
    {
        public Texture2D texture;
        public Vector2 position;
        

        public Sprite()
        {

        }

        public Sprite(Texture2D _texture, Vector2 _position)
        {
            this.texture = _texture;
            this.position = _position;
        }
    }
}